function hola(nombre) {
    //console.log('Hola, soy una funcion asincrona');
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log('Hola ' + nombre);
            resolve(nombre)
        }, 1000)
    })
}

function hablar() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log("Bla bla bla bla ...")
            resolve()
        })
    })

}

function adios(nombre) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log('Adios ' + nombre)
            resolve(nombre)
        }, 1000)
    })
}

console.log('Iniciando el proceso...')
hola('leoliss')
    .then(hablar)
    .then(hablar)
    .then(hablar)
    .then(adios)
    .then((nombre) => {
        console.log('Terminando Procesos...')
    })