async function hola(nombre) {
    //console.log('Hola, soy una funcion asincrona');
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log('Hola ' + nombre);
            resolve(nombre)
        }, 1000)
    })
}

async function hablar() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log("Bla bla bla bla ...")
            resolve()
        })
    })

}

async function adios(nombre) {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            console.log('Adios ' + nombre)
            resolve(nombre)
        }, 1000)
    })
}

async function main() {
    let nombre  = await hola('leoliss')
    await hablar()
    await hablar()
    await hablar()
    await adios(nombre)
    console.log('Termina el proceso')
}

main();